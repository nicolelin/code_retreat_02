import java.io.*;
import java.util.*;

public class MysteryApp {
    public static String output = "";

    public void start() {

    }

    public static void saveOutput(String s) {
        System.out.println(s);
        output += s + "\n";
    }

    public static void main(String[] args) {
//        MysteryApp newGame = new MysteryApp();
//        newGame.start();

        // START GAME //
        /*Initialise secret code to blank string*/
        String secretCode = "";

        /*Display welcome message*/
        saveOutput("Welcome to Bulls and Cows");
        saveOutput("======================================");

        int typeOfCode = 1; // default guess mode to user input not file input

        // USER INPUT THEIR SECRET NUMBER //
        /*Get user input for user's secret code*/
        while (true) {
            saveOutput("Please enter your secret code: (4 digits) ");
            secretCode = (new Scanner(System.in)).nextLine();
            output += secretCode + "\n"; // this will save the user input, not print it on screen. we need this separate output += because we don't want to print it again
            if (useIt(secretCode, typeOfCode)) {
                break;
            }
            saveOutput("Invalid secret code.");
        }


        // START GUESS //
        // Initliase
        String strategyCode = "numbers";
        Strategy strategy = new Strategy(); // make new strategy for the computer, runs method inside (incl. permutations)

        saveOutput("======================================");

        List<String> guesses = new ArrayList<>(); // create list of guesses
        boolean readFromFile = false;

        while (true) {
            System.out.print("Do you want to: 1. use Keyboard, or 2. read from file? ");

            try {
                int choice = Integer.parseInt((new Scanner(System.in)).nextLine());
                output += choice + "\n"; // keep, because we don't want to print this on screen

                if (choice == 1) {
                    break;
                }

                if (choice == 2) { // grab name of guesses file to input guesses
                    System.out.print("Please enter the file name: ");
                    String filename = (new Scanner(System.in)).nextLine();
                    output += filename + "\n"; // keep, because we don't want to print this on screen
                    guesses = new ArrayList<>();

                    try (BufferedReader bR = new BufferedReader(new FileReader(filename))) {
                        String guess;
                        while ((guess = bR.readLine()) != null) {
                            guesses.add(guess);
                        }
                    }

                    readFromFile = true;
                    break;
                }


            } catch (NumberFormatException e) {
                // do nothing
                saveOutput("Invalid input. Please enter 1 or 2 only.");


            } catch (IOException e) {
                readFromFile = false;
                saveOutput("File cannot be found or something is wrong! Please try again.");
            }

        }
        saveOutput("======================================");

        saveOutput("Ready? Let's go!");

        Guesser computer = new Guesser("computer", null, strategy, strategyCode);
        Guesser player = new Guesser("human", secretCode, null, null);

        int count = 0;
        while (count < 7) {
            saveOutput("**********");

            saveOutput("Round " + (count + 1));

            saveOutput("**********");

            String guess = "";
            if (readFromFile) { // read from file
                guess = guesses.get(count);
                saveOutput("Your guess: " + guess);
            } else {
                while (true) { // read from user input
                    System.out.print("Your guess: ");
                    guess = (new Scanner(System.in)).nextLine();
                    output += guess + "\n"; // keep
                    if (useIt(guess, typeOfCode)) {
                        break;
                    }
                    saveOutput("Invalid guess.");
                }
            }

            int[] playerMatch = computer.matchSecretCode(guess);
            if (playerMatch[0] == 4) {
                saveOutput("You win! :)");
                break;
            }
            String playerResult = player.informOfMatchResults(playerMatch);
            saveOutput(playerResult);
            saveOutput("--------------------");
            String computerGuess = computer.makeGuess();
            saveOutput("Computer guess: " + computerGuess);
            int[] computerMatch = player.matchSecretCode(computerGuess);
            if (computerMatch[0] == 4) {
                saveOutput("Computer win! :)");
                break;
            }
            String computerResult = computer.informOfMatchResults(computerMatch);
            saveOutput(computerResult);
            count++;
        }
        if (count == 7) {
            saveOutput("Draw!");
        }
        saveOutput("======================================");
        output += "Thank you for playing! Goodbye!\n"; // keep

        while (true) {
            System.out.print("Do you want to save the results to a file? 1. Yes 2. No: ");
            try {
                int choice = Integer.parseInt((new Scanner(System.in)).nextLine());
                if (choice == 1) {
                    System.out.print("Please enter a file name: ");
                    String filename = (new Scanner(System.in)).nextLine();
                    try (BufferedWriter bW = new BufferedWriter(new FileWriter(filename))) {
                        bW.write(output);
                    }
                    break;
                }
                if (choice == 2) {
                    break;
                }
                System.out.println("Invalid input. Please enter 1 or 2 only.");
            } catch (NumberFormatException e) {
                // do nothing
            } catch (IOException e) {
                System.out.println("File not found. Please try again.");
            }
        }
        System.out.println("Thank you for playing! Goodbye!");
    }


    private static boolean useIt(String code, int typeOfCode) {
        String pattern = typeOfCode == 1 ? "([0-9]{4})$" : "([A-F]{4})$";

        boolean unique = true;

        for (int i = 0; i < code.length(); i++) {
            for (int j = i + 1; j < code.length(); j++) {
                if (code.charAt(i) == code.charAt(j)) {
                    unique = false;
                }
            }
        }

        return code.matches(pattern) && codeOk(code);
    }

    private static boolean codeOk(String code) {
        for (int i = 0; i < code.length(); i++) {
            for (int j = i + 1; j < code.length(); j++) {
                if (code.charAt(i) == code.charAt(j)) {
                    return false;
                }
            }
            return true;
        }

        return true;
    }
}

