import java.util.*;

/**
 * Created by jlee512 on 22/05/2017.
 */
class Strategy {
    private Set<String> possibleCodes;
    private String g;
    private Set<String> memory;

    public Strategy() {
        this.memory = new LinkedHashSet<>();
        this.possibleCodes = new HashSet<>();
        getPermutations(this.possibleCodes, "", "0123456789", 4); // method which takes the string "012..." to generate random code
    }

    // generate all possible 4 digit non repeating codes
    private void getPermutations(Set<String> permutationsSet, String possibleInt, String possibleDigitsString, int secretCodeLength) {
        int length = possibleInt.length();
        // Generates all possible four digit codes
        if (length == secretCodeLength) {
            permutationsSet.add(possibleInt);
        } else {
            for (int i = 0; i < possibleDigitsString.length(); i++) {
                getPermutations(permutationsSet, possibleInt + possibleDigitsString.charAt(i), possibleDigitsString.substring(0, i) + possibleDigitsString.substring(i + 1, possibleDigitsString.length()), secretCodeLength);
            }
        }
    }

    public String getCode(int[] m) {
        if (g == null) { // if no number generated
            g = getRandomCode(); // call getRandomCode to generate 4 digit number
            return g; // return the random number
        }

        Iterator<String> iterator = possibleCodes.iterator(); // iterate through permutations
        String tempGuess = "";

        while (iterator.hasNext()) {
            String possibleGuess = iterator.next();
            int[] guessMatch = matchCodeAndGuess(possibleGuess, g);
            if (guessMatch[0] != m[0] || guessMatch[1] != m[1]) {
                iterator.remove();
            } else {
                tempGuess = possibleGuess;
            }
        }

        g = tempGuess;
        return g;
    }

    public String getRandomCode() {
        String randomCode = "";
        ArrayList<Integer> randomArray = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) { // add 0 to 9 into arraylist
            randomArray.add(i);
        }

        Collections.shuffle(randomArray); // shuffle order of Integers inside arraylist

        for (int i = 0; i < 4; i++) { // take the first 4 integers from arraylist
            randomCode += randomArray.get(i);
        }
        return randomCode; // random number with 4 digits
    }

    private int[] matchCodeAndGuess(String guess, String code) {
        int bulls = 0;
        int cows = 0;
        for (int i = 0; i < code.length(); i++) {
            if (guess.charAt(i) == code.charAt(i)) {
                bulls++;
            } else if (guess.contains(code.charAt(i) + "")) {
                cows++;
            }
        }
        return new int[] {bulls, cows};
    }

    public String getCode(String s) {
        if (s.equals("numbers")) {
            String randomCode = "";
            String code = "";

            ArrayList<Integer> randomArray = new ArrayList<Integer>();
            for (int i = 0; i < 10; i++) {
                randomArray.add(i);
            }

            Collections.shuffle(randomArray);

            for (int i = 0; i < 4; i++) {
                randomCode += randomArray.get(i);
            }
//            return randomCode;
//        } else if (s.equals("letters")) { // never called
//            String randomCode = "";
//            ArrayList<Character> randomArray = new ArrayList<>();
//            for (char i = 65; i < 71; i++) {
//                randomArray.add(i);
//            }
//
//            Collections.shuffle(randomArray);
//
//            for (int i = 0; i < 4; i++) {
//                randomCode += randomArray.get(i) + "";
//            }
//            return randomCode;
//        } else if (s.equals("memory")) { // never called
//
//            ArrayList<Integer> randomArray = new ArrayList<Integer>();
//            for (int i = 0; i < 10; i++) {
//                randomArray.add(i);
//            }
//
//            Collections.shuffle(randomArray);

            for (int i = 0; i < 4; i++) {
                code += randomArray.get(i);
            }

            while (!memory.add(code)) {
                code = "";

                ArrayList<Integer> randomArray2 = new ArrayList<Integer>();
                for (int i = 0; i < 10; i++) {
                    randomArray2.add(i);
                }

                Collections.shuffle(randomArray2);

                for (int i = 0; i < 4; i++) {
                    code += randomArray2.get(i);
                }
            }
            return code;
        }

        return null;
    }
}
